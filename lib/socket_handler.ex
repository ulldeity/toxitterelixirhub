defmodule WebsocketApp.SocketHandler do
  @behaviour :cowboy_websocket

  def init(request, _state) do
    state = %{registry_key: request.path}

    {:cowboy_websocket, request, state}
  end

  def websocket_init(state) do
    Registry.WebsocketApp
    |> Registry.register(state.registry_key, {})

    {:ok, state}
  end

  def websocket_handle({:text, json}, state) do
    Registry.WebsocketApp
    |> Registry.dispatch(state.registry_key, fn(entries) ->
      for {pid, _} <- entries do
        if pid != self() do
          Process.send(pid, json, [])
        end
      end
    end)

    {:reply, {:text, json}, state}
  end

  def websocket_info(info, state) do
    {:reply, {:text, info}, state}
  end
end